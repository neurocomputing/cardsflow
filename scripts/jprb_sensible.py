import rospy
from sensor_msgs.msg import JointState
from roboy_middleware_msgs.msg import MotorCommand, JointStatus
from roboy_middleware_msgs.srv import ControlMode, ControlModeRequest

from math import tanh, sqrt, sin
import numpy as np

import pdb
import time

class swdata:
  def __init__(self, polarity, type):
    self.polarity = polarity
    self.type = type

def softmax(x):
    return 1.0/(1+np.exp(-x))

rospy.init_node("musctest")

p = rospy.Publisher('/roboy/middleware/MotorCommand', MotorCommand, queue_size=1)
rospy.loginfo("Changing control mode to DISPLACEMENT")
change_control_mode = rospy.ServiceProxy('/roboy/shoulder_left/middleware/ControlMode', ControlMode)

max_cmd = [100,20]

#JPRB: definitions
maxV = 100000
max_vel = [maxV,maxV]
max_pos = [100000, 20000]
alpha_t = 45
beta_t = 45
sign = 1
waiting_t = 5 # time used to wait for tendons to be set up

# release speed ratio (agonistic vs antagonistic motors)
global rsr
rsr = 0.95

# State of both Segments (Primary and Secondary)
global primaryState
primaryState = 0

global overallState
overallState = 0

global swd
swd = swdata(0,0)

# Current Motor Positions
global curPos
curPos = [100000, 100000, 100000, 100000]


req = ControlModeRequest()
req.motor_id = [0,1,2,3]
req.control_mode = 2
change_control_mode(req)
rospy.loginfo("Changed to DISPLACEMENT control mode")

# Initialization of Primary Segment
msg = MotorCommand()
msg.motors = [0, 1]
msg.set_points = [10, 10]
msg.id = 3
time.sleep(1) # JPRB: do we need this?
p.publish(msg)
rospy.loginfo("Init started on Primary Segment ...")
time.sleep(waiting_t)
rospy.loginfo("Init done on Primary Segment")

# Initialization of Secondary Segment
msg = MotorCommand()
msg.motors = [2, 3]
msg.set_points = [10, 10]
msg.id = 3
time.sleep(1) # JPRB: do we need this?
p.publish(msg)
rospy.loginfo("Init started on Secondary Segment ...")
time.sleep(waiting_t)
rospy.loginfo("Init done on Secondary Segment")


# JPRB: changing M[0..1] to velocity mode
req = ControlModeRequest()
req.motor_id = [0,1]
req.control_mode = 1
change_control_mode(req)
rospy.loginfo("Changed M[0..1] to VELOCITY control mode")

msg = MotorCommand()
msg.motors = [0, 1]
msg.set_points = [0, 0]
msg.id = 3
time.sleep(1) # JPRB: do we need this?
p.publish(msg)

msg.set_points = []
msg.motors = []

def encoderticks2degrees(ticks):
    return ticks*(360.0/4096.0)

j = 0
center = [2673.5, 2539.0]
motors_map = {0: [0,1], 1: [2,3]}
msg.set_points = []


def switchModes():

    # Polarity = 1: moving towards the right
    # Polarity = -1: moving towards the left

    # Type = 1: Primary Segment moving
    # Type = 2: Secondary Segment moving

    global swd

    msg = MotorCommand()
    msg.id = 3

    if swd.type == 1:
        req = ControlModeRequest()
        req.motor_id = [0,1]
        req.control_mode = 1
        change_control_mode(req)
        rospy.loginfo("Changed M[0..1] to VELOCITY control mode")


        req = ControlModeRequest()
        req.motor_id = [2,3]
        req.control_mode = 0
        change_control_mode(req)
        rospy.loginfo("Changed M[2..3] to POSITION control mode")


        msg.set_points = [swd.polarity*maxV, -rsr*swd.polarity*maxV]
        msg.motors = [0, 1]

    if swd.type == 2:
        req = ControlModeRequest()
        req.motor_id = [0,1]
        req.control_mode = 0
        change_control_mode(req)
        rospy.loginfo("Changed M[0..1] to POSITION control mode")

        req = ControlModeRequest()
        req.motor_id = [2,3]
        req.control_mode = 1
        change_control_mode(req)
        rospy.loginfo("Changed M[2..3] to VELOCITY control mode")
        msg.set_points = [swd.polarity*maxV, -rsr*swd.polarity*maxV]
        msg.motors = [2, 3]

    p.publish(msg)



def cbSwingTwoSegments(data):
    global max_vel, j, rsr, swd
    global primaryState
    global overallState
    angles = [] # in degrees [primary, secondary]
    for i in [0,1]: #range(len(center)):
        #pdb.set_trace()
        encoderticks = data.absAngles[i] - center[i]
        angle = encoderticks2degrees(encoderticks)
        angles.append(angle)
        motors = motors_map[i]
        msg.motors.extend(motors)


        if i == 0:
            alpha = angle
        else:
            beta = angle


        if overallState == 0:
            # This is the Initial primaryState
            # Go to primaryState 1 unconditionally
            # Start swinging primary Segment away from equilibirum
            swd.polarity = 1
            swd.type = 1
            switchModes()
            overallState = 1
            rospy.loginfo("Switched to State " + str(overallState))

        elif overallState == 1:
            # Primary Segment is swinging away from equilibirum
            # Secondary Segment is locally static
            # If limit reached:
            if alpha > alpha_t:
                # Stop swinging on Primary Segment
                # Start swinging Secondary Segment away from equilibirum
                swd.polarity = 1
                swd.type = 2
                switchModes()
                overallState = 2
                rospy.loginfo("Switched to State " + str(overallState))
            else:
                # Keep things as they are
                swd.polarity = 1
                swd.type = 1
                switchModes()

        elif overallState == 2:
            # Primary Segment is locally static
            # Secondary Segment is swinging away from equilibirum
            # If limit reached:
            if beta > beta_t:
                # Keep Primary Segment static
                # Invert Swinging direction on Secondary Segment
                swd.polarity = -1
                swd.type = 2
                switchModes()
                overallState = 3
                rospy.loginfo("Switched to State " + str(overallState))
            else:
                # Keep things as they are
                swd.polarity = 1
                swd.type = 2
                switchModes()

        elif overallState == 3:
            # Primary Segment is locally static
            # Secondary Segment is swinging towards equilibirum
            # If limit reached:
            if beta < 0:
                # Start swinging Primary Segment towards equilibirum
                # Stop swinging on Secondary Segment
                swd.polarity = -1
                swd.type = 1
                switchModes()
                overallState = 4
                rospy.loginfo("Switched to State " + str(overallState))
            else:
                # Keep things as they are
                swd.polarity = -1
                swd.type = 2
                switchModes()

        elif overallState == 4:
            # Primary Segment is swinging away from equilibirum
            # Secondary Segment is locally static
            # If limit reached:
            if alpha < -alpha_t:
                # Stop swinging on Primary Segment
                # Start swinging Secondary Segment away from equilibirum
                swd.polarity = -1
                swd.type = 2
                switchModes()
                overallState = 5
                rospy.loginfo("Switched to State " + str(overallState))
            else:
                # Keep things as they are
                swd.polarity = -1
                swd.type = 1
                switchModes()

        elif overallState == 5:
            # Primary Segment is locally static
            # Secondary Segment is swinging away from equilibirum
            # If limit reached:
            if beta < -beta_t:
                # Keep Primary Segment static
                # Invert Swinging direction on Secondary Segment
                swd.polarity = 1
                swd.type = 2
                switchModes()
                overallState = 6
                rospy.loginfo("Switched to State " + str(overallState))
            else:
                # Keep things as they are
                swd.polarity = -1
                swd.type = 2
                switchModes()

        elif overallState == 6:
            # Primary Segment is locally static
            # Secondary Segment is swinging towards equilibirum
            # If limit reached:
            if beta > 0:
                # Start swinging Primary Segment towards equilibirum
                # Stop swinging on Secondary Segment
                swd.polarity = 1
                swd.type = 1
                switchModes()
                overallState = 1
            else:
                # Keep things as they are
                swd.polarity = 1
                swd.type = 2
                switchModes()

        else:
            # go to initial primaryState
            overallState == 0

    if j%1000:
        rospy.loginfo("primaryState : " + str(primaryState))
        rospy.loginfo("Angle: " + str(angle))
    j += 1
    p.publish(msg)
    #rospy.loginfo("Current joint angle values: " + str(angles))
    # rospy.loginfo("Published MotorCommand: " + str(msg))
    msg.set_points = []
    msg.motors = []


def cbSwingOneSegment(data):
    global max_vel, j, rsr
    global primaryState
    angles = [] # in degrees [primary, secondary]
    for i in [0]: #range(len(center)):
        #pdb.set_trace()
        encoderticks = data.absAngles[i] - center[i]
        angle = encoderticks2degrees(encoderticks)
        angles.append(angle)
        motors = motors_map[i]
        msg.motors.extend(motors)
        if i == 0:
            alpha = angle
        if primaryState == 0:
            # This is the Initial primaryState
            # Go to primaryState 1 unconditionally
            primaryState = 1
            # Start swinging primary Segment away form equilibirum
            msg.set_points.extend([max_vel[0], -1*rsr*max_vel[0]])


        elif primaryState == 1:
            # Primary Segment is swinging away from equilibirum
            # If limit reached:
            if alpha > alpha_t:
                # Invert swinging direction
                msg.set_points.extend([-1*max_vel[0]*rsr, max_vel[0]])
                primaryState = 2
            else:
                msg.set_points.extend([max_vel[0], -1*rsr*max_vel[0]])

        elif primaryState == 2:
            # Primary Segment is swinging towards equilibirum
            # If limit reached:
            if alpha < -alpha_t:
                # Invert swinging direction
                msg.set_points.extend([max_vel[0], -1*rsr*max_vel[0]])
                primaryState = 1
            else:
                msg.set_points.extend([-1*rsr*max_vel[0], max_vel[0]])
        else:
            # go to initial primaryState
            primaryState == 0

    if j%1000:
        rospy.loginfo("primaryState : " + str(primaryState))
        rospy.loginfo("Angle: " + str(angle))
    j += 1
    p.publish(msg)
    #rospy.loginfo("Current joint angle values: " + str(angles))
    # rospy.loginfo("Published MotorCommand: " + str(msg))
    msg.set_points = []
    msg.motors = []



def cb2(data):
    angles = [] # in degrees [lower, upper]
    for i in [0,1]: #range(len(center)):
        encoderticks = data.absAngles[i] - center[i]
        angle = encoderticks2degrees(encoderticks)
        angles.append(angle)
        motors = motors_map[i]
        msg.motors.extend(motors)
        if angle<0:
            msg.set_points.extend([abs(sin(angle)*max_cmd[i]),0])
        else:
            msg.set_points.extend([0, abs(sin(angle)*max_cmd[i])])

    #p.publish(msg)
    rospy.loginfo("Current joint angle values: " + str(angles))
    rospy.loginfo("Published MotorCommand: " + str(msg))
    msg.set_points = []
    msg.motors = []


def cb(data):
    msg.set_points = [0,0,0,0]

    if data.position[1]>0:
        msg.set_points[1] = sin(data.position[1])*max_cmd
    else:
        msg.set_points[0] = sin(abs(data.position[1]))*max_cmd

    if data.position[0]>0:
        msg.set_points[2] = sin(data.position[0])*5
    else:
        msg.set_points[3] = sin(abs(data.position[0]))*5
    p.publish(msg)

s_hw = rospy.Subscriber('/roboy/middleware/JointStatus', JointStatus, cbSwingTwoSegments, buff_size=1)
s = rospy.Subscriber('/joint_primaryStates', JointState, cb)
rospy.spin()
